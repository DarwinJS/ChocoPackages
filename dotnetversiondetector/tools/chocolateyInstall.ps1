
$ErrorActionPreference = 'Stop';

$packageName= 'dotnetversiondetector' # arbitrary name for the package, used in messages
$toolsDir   = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"

$packageArgs = @{
  packageName   = $packageName
  unzipLocation = $toolsDir
  fileType      = 'EXE_MSI_OR_MSU' #only one of these: exe, msi, msu
  url           = "$toolsDir\netver.zip"
  url64bit      = "$toolsDir\netver.zip"
  #file         = "$toolsDir\netver.zip"

  softwareName  = 'dotnetversiondetector*' #part or all of the Display Name as you see it in Programs and Features. It should be enough to be unique
  checksum      = '5A8C025D69306C369E303237DFCEF51FFCB4E77C604DEB9920080EAF9AAD58C4'
  checksumType  = 'sha256' #default is md5, can also be sha1, sha256 or sha512
  checksum64    = '5A8C025D69306C369E303237DFCEF51FFCB4E77C604DEB9920080EAF9AAD58C4'
  checksumType64= 'sha256' #default is checksumType
}

Install-ChocolateyZipPackage @packageArgs # https://chocolatey.org/docs/helpers-install-chocolatey-zip-package

Move-Item $toolsdir\dotnet.exe $toolsdir\dotnetversions.exe -Force


Write-Output ""
Write-Output "**********************************************************************************************************"
Write-Output "*  INSTRUCTIONS: At a shell prompt, type 'dotnetversions' to start the dot net version detector utility          *"
Write-Output "**********************************************************************************************************"
Write-Output ""
