      7.7.1.1
        - contains version: 7.7.1.0p1-Beta
      7.7.0.1
        - contains version: 7.7.0.0p1-Beta
      7.6.1.1
        - contains version: 7.6.1.0p1-Beta
        - New parameter AlsoLogToFile enables file logging in addition to default of new ETW / Winodows EventLogging
        - ensured barebonesinstaller.ps1 can take -SSHAgentFeature
        - removed unused parameter DisableKeyPermissionsReset throughout
        - removed DeleteServerKeysAfterInstalled
        - no longer sets log level to QUIET
        - updated to configure ETW logging
        - switch to enable file based logging
        - known issue - updating ETW on Nano is not currently working, so the package forces file logging to be enabled for
      7.6.0.1
        - Fixed problems with install parameter /SSHAgentFeature
      1.0.0.20180202
        - Set-SSHDefaultShell.ps1 - fix corrupt characters
      1.0.0.20180201
        - Set-SSHDefaultShell.ps1 - creates OpenSSH registry key if it is not yet present
      1.0.0.20180131
        - no longer automatically adds ssh-agent when installing sshd.  You must specify /SSHAgentFeature if you want it installed.
      1.0.0.0
        - complies with new changes to installation for 1.0.0.0 and attempts migration of pre-1.0.0.0 items.
        - removed switch $DisableKeyPermissionsReset
        - removed switch $UseNTRights and ntrights.exe from package
        - uses new 1.0.0.0 model for service users and permissions
        - no longer explicitly sets TERM for new installs (unless you specify the parameter) - instead relies on default behavior of sshd.exe
      0.0.24.0
        - Fixed bug in uninstall (https://github.com/DarwinJS/ChocoPackages/issues/52)
        - Uses new code from OpenSSH project for adding special privileges (is more compatible with newer Nano releases)
      0.0.23.0
        - None
      0.0.22.0
        - Fix error when uninstall runs and openssh folder is already removed: https://github.com/DarwinJS/ChocoPackages/issues/49
        - Fix error using PSH 5 Package Management to install on Server 2016: https://github.com/DarwinJS/ChocoPackages/issues/47
        - Added switches for Default Shell configuration - for both chocolatey and barebonesinstall.ps1 
          (PathSpecsToProbeForShellEXEString, SSHDefaultShellCommandOption and AllowInsecureShellEXE)
          Make the latest powershell core or windows powershell the default shell:
          /PathSpecsToProbeForShellEXEString:$env:programfiles\PowerShell\*\pwsh.exe;$env:programfiles\PowerShell\*\Powershell.exe;c:\windows\system32\windowspowershell\v1.0\powershell.exe"
          -PathSpecsToProbeForShellEXEString "$env:programfiles\PowerShell\*\pwsh.exe;$env:programfiles\PowerShell\*\Powershell.exe;c:\windows\system32\windowspowershell\v1.0\powershell.exe"
          Rules and More Examples: https://github.com/DarwinJS/ChocoPackages/blob/master/openssh/tools/Set-SSHDefaultShell.ps1
